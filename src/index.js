import * as FlexPlugin from 'flex-plugin';
import CustomlayoutPlugin from './CustomlayoutPlugin';

FlexPlugin.loadPlugin(CustomlayoutPlugin);
