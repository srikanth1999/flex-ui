import React from 'react';

import { CustomTaskListComponentStyles } from './CustomTaskList.Styles';
import { withTaskContext } from '@twilio/flex-ui';
import { Dialog, DialogActions, DialogContent, DialogContentText, DialogTitle, Button } from '@material-ui/core';

// const CustomTaskList = (props) => {
//   if (!props.isOpen) {
//     return null;
//   }

//   const { task } = props;
//   const phoneOut = task.attributes.outbound_to;
//   var isPhoneTrue = false;
//   console.log("Phone :" + phoneOut);
//   console.log("Worker : "+task.workerSid);
//   if (phoneOut == undefined) {
//     //isPhoneTrue = true;
//     fetch(
//       'https://fandango-platypus-7667.twil.io/getWorkerData?worker=' + task.workerSid
//     )
//       .then(res => res.text())
//       .then(res => {
//         console.log("worker");
//         console.log(res);
//         if(res.includes("skills") && res.includes("ASL")) {
//           console.log("worker data", res);  
//           isPhoneTrue = true; 
//         }
//       })
//       .catch(error => console.log(error));
//   }

//   const sendSMS = (event) => {
//     event.target.disabled = true;
//     if (task.attributes.outbound_to != undefined) {
//       fetch(
//         'https://fandango-platypus-7667.twil.io/message?phone=' + task.attributes.outbound_to
//       )
//         .then(res => res.json())
//         .then(res => {
//           console.log("task data", res);  
//         })
//         .catch(error => console.log(error));
//     } else {
//       console.log("Cannot send");
//     }
//   }

//   if (isPhoneTrue) {
//     return (
//       <CustomTaskListComponentStyles onClick={sendSMS}>
//         Send Link
//       </CustomTaskListComponentStyles>
//     );
//   } else {
//     return <></>;
//   }
// };
class CustomTaskList extends React.Component {
  constructor(props) {
    super(props)
   
    this.sendSMS = this.sendSMS.bind(this);
    const { task } = this.props;
    let phoneOut;
    if(task.attributes.direction == 'outbound') {
       phoneOut = task.attributes.outbound_to;
    } 
    if(task.attributes.direction == 'inbound') {
      phoneOut = task.attributes.from;
   } 
   let ownerId = "";
   if(task.attributes.owner_id !="") {
     ownerId = task.attributes.owner_id;
   }
   console.log(task.attributes);
    //var isPhoneTrue = false;
    this.state = {
      isPhoneTrue: false,
      outbound : phoneOut,
      ownerId : ownerId
    }
    console.log("Phone :" + phoneOut);
    console.log("Worker : " + task.workerSid);
    if (phoneOut != undefined) {
      //isPhoneTrue = true;
      fetch(
        'https://fandango-platypus-7667.twil.io/getWorkerData?worker=' + task.workerSid
      )
        .then(res => res.json())
        .then(res => {
          console.log("worker");
          if (JSON.parse(res).skills.includes("ASL")) {
            console.log("worker data", JSON.parse(res).skills);
            this.setState({ isPhoneTrue: true });
          }
        })
        .catch(error => console.log(error));
    }
  }
  sendSMS = (event) => {
    console.log(this.props.task.workerSid);
    event.target.disabled = true;
    if (this.state.outbound != undefined) {
      fetch(
        'https://fandango-platypus-7667.twil.io/message?phone='+this.state.outbound+'&ownerId='+this.state.ownerId+'&workerSid='+this.props.task.workerSid
      )
        .then(res => res.json())
        .then(res => {
          console.log("task data", res);
        })
        .catch(error => console.log(error));
    } else {
      console.log("Cannot send");
    }
  }
  render() {
    if (this.state.isPhoneTrue) {
      return (
        <CustomTaskListComponentStyles onClick={this.sendSMS}>
          Send Link
        </CustomTaskListComponentStyles>
      );
    } else {
      return <></>;
    }
  }
}

export default withTaskContext(CustomTaskList);

//export default CustomTaskList;



