import { default as styled } from 'react-emotion';

export const CustomTaskListComponentStyles = styled('button')`
 
margin: auto;
width: 100px;
padding : 5px; 
margin-right : 20px;
color : white;
background-color: #4CAF50;
text-align: center;
border-radius : 30px;
border : none;
cursor : pointer;
&:disabled{outline:0 !important;background-color: grey;cursor:none;}
`;
