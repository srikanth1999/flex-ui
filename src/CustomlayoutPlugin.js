import React from 'react';
import { VERSION } from '@twilio/flex-ui';
import { FlexPlugin } from 'flex-plugin';
import { Tab } from '@twilio/flex-ui'
import CustomTaskListContainer from './components/CustomTaskList/CustomTaskList.Container';
import reducers, { namespace } from './states';
//import { closeTask } from './components/CustomTaskList/CustomTaskList';

const PLUGIN_NAME = 'CustomlayoutPlugin';

export default class CustomlayoutPlugin extends FlexPlugin {
  constructor() {
    super(PLUGIN_NAME);
  }

  /**
   * This code is run when your plugin is being started
   * Use this to modify any UI components or attach to the actions framework
   *
   * @param flex { typeof import('@twilio/flex-ui') }
   * @param manager { import('@twilio/flex-ui').Manager }
   */
  init(flex, manager) {
    this.registerReducers(manager);
    const options = { sortOrder: 1 };
    // flex.TaskCanvasTabs.Content.add(
    //   <Tab key="demo-component">
    //    <CustomTaskListContainer  />
    //   </Tab>,options
    // );
    flex.TaskCanvasHeader.Content.add(<CustomTaskListContainer key="demo-component" />, options);
    flex.Actions.addListener("afterCompleteTask", (payload) => {
      console.log("payload");
      console.log(payload);
      console.log(payload.task);
      console.log("MapId :"+payload.task.attributes.mapId);
      console.log("Room Name"+payload.task.attributes.videoChatRoom);
      console.log("Task Sid :"+payload.task.taskSid);
      if(payload.task.attributes.mapId != undefined) {
      return fetch(
        `https://fandango-platypus-7667.twil.io/deletevideotask?taskId=${payload.task.taskSid}&mapId=${payload.task.attributes.mapId}&key=${payload.task.attributes.videoChatRoom}`
      )
      .then(res => res.json())
        .then(res => {
          console.log("task data", res);
        })
        .catch(error => console.log(error));
      }
    });
    let alertSound = new Audio("https://www.soundhelix.com/examples/mp3/SoundHelix-Song-1.mp3");
    alertSound.loop = true;

    const resStatus = ["accepted","canceled","rejected","rescinded","timeout"];

    manager.workerClient.on("reservationCreated", function(reservation) {
      if (reservation.task.taskChannelUniqueName === 'video') {
        alertSound.play()
      };
      resStatus.forEach((e) => {
        reservation.on(e, () => {
          alertSound.pause()
        });
      });
    });
  }

  /**
   * Registers the plugin reducers
   *
   * @param manager { Flex.Manager }
   */
  registerReducers(manager) {
    if (!manager.store.addReducer) {
      // eslint: disable-next-line
      console.error(`You need FlexUI > 1.9.0 to use built-in redux; you are currently on ${VERSION}`);
      return;
    }

    manager.store.addReducer(namespace, reducers);
  }
}
